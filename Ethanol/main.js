// Preloader

window.addEventListener('load', function(){ 
  let elem = document.querySelector("#loader");
  document.querySelector("#loader").style.cssText = "display: none";
}, false);


// Features Section (switch info in click)

let refreshItems = () => {
  document.querySelectorAll('.feature__item').forEach(btn => btn.classList.remove('feature__item--active'));
  document.querySelectorAll('.feature__info').forEach(info => info.style.display = 'none');
};

document.querySelectorAll('.feature__item')
  .forEach(btn => btn.addEventListener('click', function () {
    let currentInfoBoxId = btn.classList[1]; // get clicked element's second  class
    let currentInfoBox = document.querySelector('#' + currentInfoBoxId);
    refreshItems();
    currentInfoBox.style.cssText = "display: flex; animation: fadeInFromNone 0.5s ease-in-out; ";
    this.classList.add('feature__item--active');
  }));


// Filter gallery 

let refreshGallery = () => {
  document.querySelectorAll('.gallery__btn').forEach(btn => btn.classList.remove('gallery__btn--active'));
};

document.querySelectorAll('.gallery__btn')
  .forEach(btn => btn.addEventListener('click', () => {
    refreshGallery();

    if (btn.id === 'all') {
      document.querySelectorAll('.gallery__container').forEach(image => image.style.display = 'block');
      btn.classList.add('gallery__btn--active');
    } else {
      let imagesClass = btn.id;
      document.querySelectorAll('.gallery__container').forEach(image => image.style.display = 'none');
      document.querySelectorAll('.' + imagesClass).forEach(image => image.style.display = 'block');
      btn.classList.add('gallery__btn--active');
    }
  }));


// Map

function initMap() {
  let lviv = { lat: 49.809543, lng: 23.978644 };
  let map = new google.maps.Map(
    document.querySelector('#map'), {
    zoom: 13,
    center: lviv,
    disableDefaultUI: true,
    styles: [
      {
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#f5f5f5"
          }
        ]
      },
      {
        "elementType": "labels.icon",
        "stylers": [
          {
            "visibility": "off"
          }
        ]
      },
      {
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "color": "#616161"
          }
        ]
      },
      {
        "elementType": "labels.text.stroke",
        "stylers": [
          {
            "color": "#f5f5f5"
          }
        ]
      },
      {
        "featureType": "administrative.land_parcel",
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "color": "#bdbdbd"
          }
        ]
      },
      {
        "featureType": "administrative.locality",
        "elementType": "labels.text",
        "stylers": [
          {
            "color": "#ffcc33"
          }
        ]
      },
      {
        "featureType": "administrative.locality",
        "elementType": "labels.text.stroke",
        "stylers": [
          {
            "color": "#ffffff"
          }
        ]
      },
      {
        "featureType": "administrative.neighborhood",
        "elementType": "geometry.fill",
        "stylers": [
          {
            "color": "#ffcc33"
          }
        ]
      },
      {
        "featureType": "administrative.neighborhood",
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "color": "#ffcc33"
          },
          {
            "weight": 3
          }
        ]
      },
      {
        "featureType": "administrative.neighborhood",
        "elementType": "labels.text.stroke",
        "stylers": [
          {
            "color": "#ffffff"
          }
        ]
      },
      {
        "featureType": "poi",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#eeeeee"
          }
        ]
      },
      {
        "featureType": "poi",
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "color": "#757575"
          }
        ]
      },
      {
        "featureType": "poi.park",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#e5e5e5"
          }
        ]
      },
      {
        "featureType": "poi.park",
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "color": "#9e9e9e"
          }
        ]
      },
      {
        "featureType": "road",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#ffffff"
          }
        ]
      },
      {
        "featureType": "road.arterial",
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "color": "#757575"
          }
        ]
      },
      {
        "featureType": "road.highway",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#dadada"
          }
        ]
      },
      {
        "featureType": "road.highway",
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "color": "#616161"
          }
        ]
      },
      {
        "featureType": "road.local",
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "color": "#9e9e9e"
          }
        ]
      },
      {
        "featureType": "transit.line",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#e5e5e5"
          }
        ]
      },
      {
        "featureType": "transit.station",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#eeeeee"
          }
        ]
      },
      {
        "featureType": "water",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#c9c9c9"
          }
        ]
      },
      {
        "featureType": "water",
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "color": "#9e9e9e"
          }
        ]
      }
    ]
  });

  let marker = new google.maps.Marker({
    position: lviv,
    icon: '../img/ico/map_marker.png',
    map: map
  });
}

// To top btn

let toTopBtn = document.querySelector('.top');

window.addEventListener('scroll', () => {
  if (window.pageYOffset > 700) {
    toTopBtn.style.cssText = "display: block; animation: scrollTop .5s ease-in-out;"
  } else {
    toTopBtn.style.cssText = "display: none; opacity: 0;"
  }
});

toTopBtn.addEventListener('click', () => {
  window.scrollTo({ top: 0, behavior: 'smooth' });

});

// Menu appers after scrolling

let mainMenu = document.querySelector('.header-section');
let logo = document.querySelector('.main-menu__logo');

window.addEventListener('scroll', () => {
  if (window.pageYOffset > 120) {
    mainMenu.style.cssText = "height: 56px; position: fixed; box-shadow: -1px 1px 3px 0px #f3f3f3;"
    logo.style.cssText = "transform: scale(0.7); margin-left: -25px;"
  } else {
    mainMenu.style.cssText = "height: 120px; position: relative;"
    logo.style.cssText = "transform: scale(1);"
  }
});


// Filled skills when user scroll to point

document.addEventListener("scroll", () => {
  let skillsSection = document.querySelector('#work');
  let rect = skillsSection.getBoundingClientRect();

  if (rect.top < window.innerHeight && rect.bottom > 0) {
    document.querySelectorAll('.work__skill--progress').forEach(skill => skill.style.cssText = "animation: skillsFilled 1.5s ease-in-out;");
  }
});
