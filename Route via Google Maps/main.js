let markers = [];
let savedRoutes = [];
let map;

let directionsRenderer = null;
let directionsService = null;

let currentRoute;
let click = 0;


function initMap() {
  directionsRenderer = new google.maps.DirectionsRenderer;
  directionsService = new google.maps.DirectionsService;

  if (localStorage.getItem('routes')) {
    savedRoutes = JSON.parse(localStorage.getItem('routes'));

    for (var i = 0; i < savedRoutes.length; i++) {
      let listWrapper = document.querySelector('#route-list');
      let routeItem = document.createElement('li');

      routeItem.setAttribute("id", i);
      routeItem.appendChild(document.createTextNode(savedRoutes[i].summary));
      listWrapper.appendChild(routeItem);
      
      routeItem.addEventListener("click", (event) => {
        let elId = event.target.id;
        clearMap();
        drawRoute(savedRoutes[elId].legs[0].start_location, savedRoutes[elId].legs[0].end_location);
        document.querySelector('#save').classList.add("disabled");
      });
      
    }
  }

  map = new google.maps.Map(document.querySelector('#map'), {
    zoom: 14,
    center: { lat: 37.77, lng: -122.447 }
  });

  // Call addMarkers() when the map is clicked
  map.addListener('click', (event) => {
    addMarkers(event.latLng);
  });

  // Add 'delete' button, when we have already saved routes in local storage
  const addRemoveBtn = () => {
      if(savedRoutes.length > 0) {
        document.querySelector('#delete').style.display = "block";
      }
  }
  addRemoveBtn();
}


const addMarkers = (location) => {
  if (markers.length >= 2) return;

  const marker = new google.maps.Marker({
    position: location,
    map: map
  });

  markers.push(marker);

  if (markers.length === 2) {
    calculateAndDisplayRoute();
  }
}

const deAttachMarkers = () => {
  for (var i = 0; i < markers.length; i++) {
    markers[i].setMap(null);
  }
}

// Removes the markers from the map and array.
const clearMap = () => {
  deAttachMarkers();
  directionsRenderer.setMap(null);
  markers = [];

  // Disabled 'save' btn
  document.querySelector('#save').classList.add("disabled");
}

// 'CLEAR' btn 
document.querySelector('#clear').addEventListener('click', () => {
  clearMap();
});

// 'DELETE' btn removes routes from local storage
document.querySelector('#delete').addEventListener('click', () => {
  localStorage.removeItem("routes");
  document.location.reload();
});

// Draw the route
const drawRoute = (origin, destination) => {
  directionsRenderer.setMap(map);
  directionsService.route({
    origin: origin,  
    destination: destination, 
    travelMode: 'DRIVING'

  }, function (response, status) {
    if (status == 'OK') {
      directionsRenderer.setDirections(response);
      currentRoute = response.routes[0];
    } else {
      window.alert('Directions request failed due to ' + status);
    }
  });

  // Activate 'save' btn (when the route was traced)
  document.querySelector('#save').classList.remove("disabled");
}


// 'SAVE' route btn
document.querySelector('#save').addEventListener('click', () => {
  let listWrapper = document.querySelector('#route-list');
  let routeItem = document.createElement('li');
  
  listWrapper.appendChild(routeItem);
  routeItem.setAttribute("id", savedRoutes.length);
  routeItem.appendChild(document.createTextNode(currentRoute.summary));
  
  
  savedRoutes.push(currentRoute);
  localStorage.setItem('routes', JSON.stringify(savedRoutes));
  
  // Add delete button on page, when first route was added
  if(savedRoutes.length > 0) {
    document.querySelector('#delete').style.display = "block";
  }

  click = 0;
  click += 1;
  if (click === 1){
    document.querySelector('#save').classList.add("disabled");
  }

  routeItem.addEventListener("click", () => {
    clearMap();
    let route = savedRoutes[routeItem.id];
    drawRoute(route.legs[0].start_location, route.legs[0].end_location);
    document.querySelector('#save').classList.add("disabled");
  });
});

const calculateAndDisplayRoute = () => {
  deAttachMarkers();
  drawRoute(markers[0].position, markers[1].position);
}